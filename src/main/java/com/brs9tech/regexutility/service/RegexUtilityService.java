package com.brs9tech.regexutility.service;

import java.io.File;
import java.io.IOException;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class RegexUtilityService {
	static final Logger logger=Logger.getLogger(RegexUtilityService.class);
	private static final String CHARSET_ENCODING = "UTF-8";
	private static final String SUFFIX = "_COPY";

	public boolean isValidFilePath(String fp) {
		BasicConfigurator.configure();
		if (fp==null || fp.isEmpty()) {
			logger.info("isValidFilePath(String fp) Input parameter is null or empty.");
			return false;
		}
		File src=new File(fp);
		boolean result=src.isFile();
		logger.info(fp + " is valid file? " + result);
		return result;
	}

	public boolean searchAndReplace(String searchString, String replaceWith, String srcFilePath) {
		BasicConfigurator.configure();
		if (searchString==null || searchString.isEmpty()) {
			logger.info("searchAndReplace(String searchString, String replaceWith, String srcFilePath) searchString is null or empty.");
			return false;
		}
		if (replaceWith==null || replaceWith.isEmpty()) {
			logger.info("searchAndReplace(String searchString, String replaceWith, String srcFilePath) replaceWith is null or empty.");
			return false;
		}
		if (!isValidFilePath(srcFilePath)) {
			logger.info("searchAndReplace(String searchString, String replaceWith, String srcFilePath) srcFilePath is null or empty or not a valid file.");
			return false;
		}
		if (searchString.equals(replaceWith) ) {
			logger.info("searchAndReplace(String searchString, String replaceWith, String srcFilePath) searchString is the same as replaceWith.");
			return false;
			
		}
		File src=new File(srcFilePath);
		try {
			String srcStr=fileToString(src);
			if (!srcStr.contains(searchString)) return false;
			File dest=new File(FilenameUtils.concat(
					FilenameUtils.getFullPath(srcFilePath),
					FilenameUtils.getBaseName(srcFilePath) + 
						SUFFIX + 
						FilenameUtils.EXTENSION_SEPARATOR_STR + 
						FilenameUtils.getExtension(srcFilePath)));
			String destStr=srcStr.replaceAll(searchString, replaceWith);
			FileUtils.write(dest, destStr, CHARSET_ENCODING);
			return (regexExistInFile(replaceWith, dest) && !regexExistInFile(searchString, dest));
		} catch (IOException|PatternSyntaxException e) {
			logger.error(e);
			return false;
		}
		
	}

	public boolean regexExistInFile(String regex, File src) {
		BasicConfigurator.configure();
		try {
			String srcStr=fileToString(src);
			boolean result=srcStr.contains(regex);
			logger.info(regex + " exist in file : "+ src.toString() + "? " + result);
			return srcStr.contains(regex);
		} catch (IOException e) {
			logger.error(e);
			return false;
		}
	}
	
	private String fileToString(File src) throws IOException {
		return FileUtils.readFileToString(src, CHARSET_ENCODING);
	}

}
