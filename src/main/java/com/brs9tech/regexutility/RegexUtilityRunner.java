package com.brs9tech.regexutility;

import java.util.Scanner;

import com.brs9tech.regexutility.service.RegexUtilityService;

public class RegexUtilityRunner {
	static final String SEARCH_AND_REPLACE_ERROR_MSG="Search and Replace failed. Check regexutility.log in the same folder as the executable program.";
	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in);
		String src=null;
		String searchString=null;
		String replaceWith=null;
		System.out.println("Regex Utility\n");
		System.out.println("Description: \nThis utility takes as input a file path, search string, replacement string.\n");
		System.out.println("The utility creates a copy of the original file and adds _COPY to the base filename and");
		System.out.println("places in same folder location as the original file.\n");
		System.out.println("The new file contents will be same as the original except that all instances of the search string");
		System.out.println("will be replaced by the replacement string.\n");
		while ((src==null || src.isEmpty()) && src!="x") {
			System.out.println("Enter a path to an existing file (Mac Eg. /Users/owner/input.txt): [enter x, to exit the program]");
			src = myObj.nextLine();
		}
		if (src.equalsIgnoreCase("x")) {
			System.out.println("Exiting the program...");
			myObj.close();
			System.exit(1);

		} else {
			System.out.println("File path is: " + src + "\n");
		}
		while ((searchString==null || searchString.isEmpty()) && searchString!="x") {
			System.out.println("Enter a string to search in the file (Eg. regex A): [enter x, to exit the program]");
			searchString = myObj.nextLine();
		}
		if (searchString.equalsIgnoreCase("x")) {
			System.out.println("Exiting the program...");
			myObj.close();
			System.exit(1);

		} else {
			System.out.println("Search string is: " + searchString + "\n");
		}
		while ((replaceWith==null || replaceWith.isEmpty()) && replaceWith!="x") {
			System.out.println("Enter a string to replace the search string in the file (Eg. regex B): [enter x, to exit the program]");
			replaceWith = myObj.nextLine();
		}
		if (replaceWith.equalsIgnoreCase("x")) {
			System.out.println("Exiting the program...");
			myObj.close();
			System.exit(1);

		} else {
			System.out.println("Replacement string is: " + replaceWith + "\n");
		}
		myObj.close();
		RegexUtilityService regexSvc=new RegexUtilityService();
		if (regexSvc.isValidFilePath(src)) {
			if (regexSvc.searchAndReplace(searchString, replaceWith, src)) {
				System.out.println("Congratulations! A new file was created and placed in same folder as the original file.");
				System.out.println("The new file has the same contents as the original file except that");
				System.out.println("the search string was replaced by the replacement string.");
				System.exit(0);
			} else {
				System.out.println(SEARCH_AND_REPLACE_ERROR_MSG);
				System.exit(1);
			}
		} else {
			System.out.println(SEARCH_AND_REPLACE_ERROR_MSG);
			System.exit(1);
		}
		

	}

}
