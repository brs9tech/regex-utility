package com.brs9tech.regexutility;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import com.brs9tech.regexutility.service.RegexUtilityService;

public class AppTest 
{
    @Test
    public void validFilePathArgTest()
    {
        String fp="/Users/owner/input.txt";
        RegexUtilityService us=new RegexUtilityService();
    	assertTrue( us.isValidFilePath(fp) );
    }
    @Test
    public void invalidFilePathArgTest()
    {
        String fp="input.rtf";
        RegexUtilityService us=new RegexUtilityService();
    	assertFalse( us.isValidFilePath(fp) );
    }
	@Test
    public void regexReplaceTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        String src="/Users/owner/input.txt";
        String searchString="regex A";
        String replaceWith="regex B";
    	assertTrue( us.searchAndReplace(searchString,replaceWith, src));
    }
	@Test
    public void regexReplaceSearchAndReplaceStringsAreSameTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        String src="/Users/owner/input.txt";
        String searchString="regex A";
        String replaceWith="regex A";
    	assertFalse( us.searchAndReplace(searchString,replaceWith, src));
    }
	@Test
    public void regexReplaceInFileNotFoundTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        String src="input.rtf";
        String searchString="regex A";
        String replaceWith="regex B";
    	assertFalse( us.searchAndReplace(searchString,replaceWith, src));
    }
    @Test
    public void regexExistInFileTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        File src=new File("/Users/owner/input_COPY.txt");
        String regex="regex B";
    	assertTrue( us.regexExistInFile(regex, src));
    }
    @Test
    public void regexNotExistInFileTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        File src=new File("/Users/owner/input_COPY.txt");
        String regex="regex A";
    	assertFalse( us.regexExistInFile(regex, src));
    }
    @Test
    public void regexExistInFileNotFoundTest()
    {
        RegexUtilityService us=new RegexUtilityService();
        File src=new File("input.rtf");
        String regex="regex B";
    	assertFalse( us.regexExistInFile(regex, src));
    }
    
}
